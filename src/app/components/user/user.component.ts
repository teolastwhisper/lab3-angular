import { Component, OnInit,Input } from '@angular/core';
import {  Observable } from 'rxjs';
import { User, UserService } from 'src/app/services/user.service';
import { GenderPipe } from 'src/app/pipe/gender.pipe';
@Component({
  selector: 'lab-js-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @Input() public lastName: string;
  @Input() public firstName: string;
  @Input() public userId: number;
  @Input() public gender: string;
  public genderPipe : GenderPipe
  public user$: Observable<User>;
  public constructor(
    private readonly userService: UserService,
  ) { }

  public ngOnInit(): void {
    this.user$ = this.userService.getCurrentUser$();
    this.user$.subscribe(obj => {

      this.lastName = obj.lastName;
      this.firstName = obj.firstName;
      this.userId = obj.id;
      this.gender = obj.gender
    }
      );
  }
  

}
